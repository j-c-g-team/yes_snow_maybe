from sklearn import svm
import numpy as np
import pickle
from sklearn.externals import joblib
classifier = joblib.load('testmodel.pkl')
import PIL
from PIL import Image


def img_to_matrix(filename, verbose=False):
    newsize = (400, 400)
    img = PIL.Image.open(filename)
    if verbose==True:
        print "changing size from %s to %s" % (str(img.size), str(newsize))
    img = img.resize(newsize)
    img = list(img.getdata())
    img = map(list, img)
    img = np.array(img)
    return img

def flatten_image(img):
    s = img.shape[0] * img.shape[1]
    img_wide = img.reshape(1, s)
    return img_wide[0]

def clean_image(image):
    data =[]
    img = img_to_matrix(image)
    img = flatten_image(img)
    data.append(img)
    return data

def predict(image):
    data = clean_image(image)
    result = classifier.predict(data)
    result = result[0]
    result = bool(result)
    return result