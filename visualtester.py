import SnowPredictor
import os
import cv2

img_dir = "imagetest/"
images = [img_dir+ f for f in os.listdir(img_dir)]

for image in images:
    try:
        snowing = SnowPredictor.predict(image)
        displayimage=cv2.imread(image)
        if snowing:
            cv2.putText(displayimage,'IT SNOWS', (200,200), cv2.FONT_HERSHEY_SIMPLEX,2,255)
        else:
            cv2.putText(displayimage,'THAT IS DIRT', (200,200), cv2.FONT_HERSHEY_SIMPLEX,2,255)
        cv2.imshow('image',displayimage)
        cv2.waitKey(5)
    except:
        pass

cv2.destroyAllWindows()


